#!/usr/bin/env python
# coding: utf-8

# In[1]:


import poes


# In[ ]:





# In[ ]:





# In[14]:


from fastapi import FastAPI
from pydantic import BaseModel
from starlette.staticfiles import StaticFiles

app = FastAPI(title="POEPILLOW API",
    description="This is an API for POEPILLOW",
    version="0.1")
app.mount("/static", StaticFiles(directory="static"), name="static")

class charactergif(BaseModel):
    accountname: str
    charname: str
    filedirc: str
    

@app.post("/creategif")
def creategif(accountname, charname):
    
    poes.getchar(accountname, charname)
    poes.downloaditems(accountname, charname)
    poes.makechardir(accountname, charname)
    poes.createtext(accountname,charname)
    poes.itemprop(accountname, charname)
    poes.itemreqs(accountname, charname)
    return(poes.creategif(accountname, charname))


# In[2]:


#charname = input('char name: ')


# In[3]:


#poes.downloaditems('artctrl', charname, '/home/pi/poe/')


# In[4]:


#poes.getchar('artctrl', charname, '/home/pi/poe/')


# In[5]:


#poes.createtext('artctrl',charname, '/home/pi/poe/')


# In[6]:


#poes.itemprop('artctrl', charname, '/home/pi/poe/')


# In[7]:


#poes.itemreqs('artctrl', charname, '/home/pi/poe/')


# In[8]:


#poes.creategif('artctrl', charname, '/home/pi/poe/')


# In[8]:


#poes.generatemicrogif()


# In[10]:


#poes.getstashTab()

