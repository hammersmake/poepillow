#!/usr/bin/env python
# coding: utf-8

# python path of exile library for accessing poe api. 
# 
# ability to create character items screens. 

# In[216]:


from slugify import slugify
import random
from PIL import ImageDraw, ImageFont, Image, ImageFilter
import shutil
import requests
import arrow
import configparser
from pymongo import MongoClient
import os
import json
import imageio
from fake_useragent import UserAgent 
import socket


# In[217]:


ipaddr = ([l for l in ([ip for ip in socket.gethostbyname_ex(socket.gethostname())[2] if not ip.startswith("127.")][:1], [[(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1]]) if l][0][0])


# In[218]:


#config parser is used to open the username and password for mongodb connection. 
config = configparser.ConfigParser()
config.sections()

config.read('config.ini')

config.sections()


# In[219]:


poepildir = config['DEFAULT']['poedir']


# In[220]:


fontdirec = config['DEFAULT']['fontdir']


# In[221]:


ua = UserAgent()
header = {'User-Agent':str(ua.chrome)}


# In[222]:


client = MongoClient("mongodb://{}:{}@cluster0-shard-00-00.uogwg.mongodb.net:27017,cluster0-shard-00-01.uogwg.mongodb.net:27017,cluster0-shard-00-02.uogwg.mongodb.net:27017/<dbname>?ssl=true&replicaSet=atlas-1s7tn5-shard-0&authSource=admin&retryWrites=true&w=majority".format(config['DEFAULT']['username'], config['DEFAULT']['password']))
db = client.test


# In[223]:


collection = db['poes']

# Insert collection
#collection.insert_one(charprof['character'])


# In[224]:


avatarfiles = ['Ascendant_avatar.png', 'Deadeye_avatar.png', 'Ranger_avatar.png', 
               'Champion_avatar.png', 'Raider_avatar.png', 'Guardian_avatar.png', 
               'Chieftain_avatar.png', 'Berserker_avatar.png', 'Marauder_avatar.png', 
               'Trickster_avatar.png', 'Duelist_avatar.png', 'Elementalist_avatar.png', 
               'Saboteur_avatar.png', 'Inquisitor_avatar.png', 'Slayer_avatar.png', 
               'Hierophant_avatar.png', 'Scion_avatar.png', 'Necromancer_avatar.png', 
               'Templar_avatar.png', 'Shadow_avatar.png', 'Gladiator_avatar.png', 
               'Pathfinder_avatar.png', 'Assassin_avatar.png', 'Occultist_avatar.png', 
               'Witch_avatar.png', 'Juggernaut_avatar.png']


# In[ ]:





# In[225]:


def setupfolder():
    #performs a setup of the folder - checking that it exists 
    #and that the character images are in it
    isdir = os.path.isdir(poepildir) 
    #print(isdir) 
    if isdir == True:
        return(True)
    else:
        os.makedirs(config['DEFAULT']['poedir'])
        for ava in avatarfiles:
            avaurl = 'http://128.199.242.244/static/temp/' + ava
            response = requests.get(avaurl, stream=True)
            with open(poepildir + "/" + ava, 'wb') as out_file:
                shutil.copyfileobj(response.raw, out_file)
                del response
        
        return('Folder {} created, with files {}'.format(poepildir, os.listdir(poepildir)))


# In[226]:


def getchar(accountname, charname):
    #specify poe account name, character
    #saves the data to mongodb. 
    #getchar('artctrl', 'pudeep')
    endpoint = "https://www.pathofexile.com/character-window/get-items"
    params = {"accountName": accountname, "character": charname}
    charprof = requests.get(endpoint, params=params, headers=header).json()
    #returns the
    for charitems in charprof['items']:
    #,mk
        charitems.update({'localfile' : '{}/{}{}'.format(poepildir, charitems['id'], '.png')})
    filter_dict = {'character.name' : charname}
    if collection.count(filter_dict):
        collection.update_one(filter_dict, {"$set": charprof})
    else:
        collection.insert_one(charprof)
    return('ok')


# In[228]:


#finda = collection.find({'character.name' : charname}).limit(1)
def searchchar(nameofchar):
    for charname in collection.find({'character.name' : nameofchar}).limit(1):
        return(charname) 
    else:
        print('Character not found')


# In[ ]:





# In[229]:


def downloaditems(accountname, charname):
    finda = collection.find({'character.name' : charname}).limit(1)
    for f in finda:
        chard = f
    #print(chard)
    for charitems in chard['items']:
        #print(charitems['icon'])
        response = requests.get(charitems['icon'], stream=True)
        with open('{}/{}{}'.format(poepildir, charitems['id'], '.png'), 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
            del response
    
    somesize = list()
    for charp in chard['items']:
        #print(charp['localfile'])
        newim = Image.open(charp['localfile'])
        somesize.append(newim.size[1])
    totsize = sum(somesize)
    charname = chard['character']['name']
    charclass = chard['character']['class']
    charlvl = str(chard['character']['level'])
    
    charleag = chard['character']['league']
                  
    fulltitle = ' class: ' + charclass + '\n level: ' + charlvl + '\n league: ' + charleag
    im = Image.new(mode = "RGB", color='black', size = (500, totsize + 266) )

    d1 = ImageDraw.Draw(im)
    myFont = ImageFont.truetype(fontdirec, 14)
    titfont = ImageFont.truetype(fontdirec, 48)
    charpic = Image.open(poepildir + '/' + charclass + '_avatar.png')
    im.paste(charpic, (300, 10))
    d1.text((10, 10), charname.upper(), font=titfont, fill =(127, 127, 127))
    d1.text((10, 80), fulltitle.upper(), font=myFont, fill =(127, 127, 127))
    stnum = 266
    for chari in chard['items']:

        newim = Image.open(chari['localfile'])

        im.paste(newim, (10, stnum))

        stnum = stnum + newim.size[1]


        #adgo.append(stnum)
        #totsize.append(im.size[1])
    im.save('{}/{}-{}.png'.format(poepildir, accountname, charname))
    return('ok')


# In[231]:


def makechardir(accountname, charname):
    try:
        os.makedirs('{}/{}/{}'.format(poepildir, accountname, charname))
    except FileExistsError:
        pass


# In[230]:


#getchar('artctrl', 'pudeep', '/home/pi/poe/')


# In[232]:


def createtext(accountname, charname):
    im = Image.open('{}/{}-{}.png'.format(poepildir, accountname, charname))
    #with open('{}{}-{}.png'.format(outdir, accountname, charname) )
    finda = collection.find({'character.name' : charname}).limit(1)
    for f in finda:
        chard = f
    stnum = 266
    d1 = ImageDraw.Draw(im)
    myFont = ImageFont.truetype(fontdirec, 16)
    for chari in chard['items']:
     
        newim = Image.open(chari['localfile'])
        

        d1.text((newim.size[0] + 10, stnum), chari['name'], font=myFont, fill =(127, 127, 127))
        #check if empty, if so put typeline at same y cords - otherwise + it. 
        d1.text((newim.size[0] + 10, stnum + 20), chari['typeLine'], font=myFont, fill =(127, 127, 127))
        stnum = stnum + newim.size[1]

    im.save('{}/{}/{}/1.png'.format(poepildir, accountname, charname))

    


# In[ ]:





# In[233]:


def createdescription(accountname, charname):
    im = Image.open('{}/{}-{}.png'.format(poepildir, accountname, charname))
    #with open('{}{}-{}.png'.format(outdir, accountname, charname) )
    finda = collection.find({'character.name' : charname}).limit(1)
    for f in finda:
        chard = f
    stnum = 266
    d1 = ImageDraw.Draw(im)
    myFont = ImageFont.truetype(fontdirec, 16)
    
    for chari in chard['items']:
        flavtxt = list()
        try:
            flavlen = (len(chari['flavourText']))
            for flav in range(0, flavlen):
                flavtxt.append(chari['flavourText'][flav])
        except KeyError: 
            pass
        newim = Image.open(chari['localfile'])
        
        try:
            d1.text((newim.size[0] + 10, stnum), '\n'.join(flavtxt), font=myFont, fill =(127, 127, 127))

            #print(chari['flavourText'][0])
        except KeyError: 
            pass
        #d1.text((newim.size[0] + 10, stnum), chari['name'], font=myFont, fill =(127, 127, 127))
        #d1.text((newim.size[0] + 10, stnum + 20), chari['typeLine'], font=myFont, fill =(127, 127, 127))
        stnum = stnum + newim.size[1]


    im.save('{}/{}/{}/4.png'.format(poepildir, accountname, charname))
    


# In[281]:


def createexmods(accountname, charname):
    im = Image.open('{}/{}-{}.png'.format(poepildir, accountname, charname))
    #with open('{}{}-{}.png'.format(outdir, accountname, charname) )
    finda = collection.find({'character.name' : charname}).limit(1)
    for f in finda:
        chard = f
    stnum = 266
    d1 = ImageDraw.Draw(im)
    myFont = ImageFont.truetype(fontdirec, 16)
    
    for chari in chard['items']:
        flavtxt = list()
        try:
            flavlen = (len(chari['explicitMods']))
            for flav in range(0, flavlen):
                flavtxt.append(chari['explicitMods'][flav])
        except KeyError: 
            pass
        newim = Image.open(chari['localfile'])
        
        try:
            d1.text((newim.size[0] + 10, stnum), '\n'.join(flavtxt), font=myFont, fill =(127, 127, 127))

            #print(chari['flavourText'][0])
        except KeyError: 
            pass
        #d1.text((newim.size[0] + 10, stnum), chari['name'], font=myFont, fill =(127, 127, 127))
        #d1.text((newim.size[0] + 10, stnum + 20), chari['typeLine'], font=myFont, fill =(127, 127, 127))
        stnum = stnum + newim.size[1]

    im.save('{}/{}/{}/5.png'.format(poepildir, accountname, charname))
    


# In[21]:


def itemprop(accountname, charname):
    #+20% Quality, 900 armor etc
    im = Image.open('{}/{}-{}.png'.format(poepildir, accountname, charname))
    finda = collection.find({'character.name' : charname}).limit(1)
    for f in finda:
        chard = f
    stnum = 266

    d1 = ImageDraw.Draw(im)
    myFont = ImageFont.truetype(fontdirec, 16)
    for charp in chard['items']:
        newim = Image.open(charp['localfile'])
        itemreql = list()
        newreqs = list()

        try:
            for cha in range(0, len(charp['properties'])):
                if '{0}' in charp['properties'][cha]['name']:
                    if '{1}' in charp['properties'][cha]['name']:
                        repl =(charp['properties'][cha]['name'].replace('{1}', charp['properties'][cha]['values'][1][0]))
                        replone = (repl.replace('{0}', charp['properties'][cha]['values'][0][0]))
                        itemreql.append(replone )


                    else:
                        replzero = (charp['properties'][cha]['name'].replace('{0}', charp['properties'][cha]['values'][0][0]))
                        itemreql.append(replzero)

                else:
                    try:
                        replnone = (charp['properties'][cha]['values'][0][0] + ' ' + charp['properties'][cha]['name'])
                        itemreql.append(replnone)
                    except IndexError:
                        replnone = charp['properties'][cha]['name']
                        itemreql.append(replnone)

        except KeyError:
            pass
        d1.text((newim.size[0] + 10, stnum + 20), '\n'.join(itemreql), font=myFont, fill =(127, 127, 127))
        stnum = stnum + newim.size[1]

    im.save('{}/{}/{}/3.png'.format(poepildir, accountname, charname))


# In[255]:


def itemreqs(accountname, charname):
    im = Image.open('{}/{}-{}.png'.format(poepildir, accountname, charname))
    #with open('{}{}-{}.png'.format(outdir, accountname, charname) )
    finda = collection.find({'character.name' : charname}).limit(1)
    for f in finda:
        chard = f
    stnum = 266
    #imagebreak = 100
    d1 = ImageDraw.Draw(im)
    myFont = ImageFont.truetype(fontdirec, 16)
    for charp in chard['items']:
        newim = Image.open(charp['localfile'])
        itemreql = list()
        newreqs = list()
        #print(charp['ilvl'])
        lvlreq = (charp['ilvl'])
        newreqs.append('ilvl: ' + str(lvlreq))
        try:
            for reqs in range(0, len(charp['requirements'])):
                    #print(charp[reqs]['requirements']['name'] + ': ' + charp[reqs]['requirements']['values'][0][0])
                    itemreqs = (charp['requirements'][reqs]['name'] + ': ' + charp['requirements'][reqs]['values'][0][0])
        
                    newreqs.append(itemreqs)
            
        except KeyError:
            pass
        d1.text((newim.size[0] + 10, stnum), '\n'.join(newreqs), font=myFont, fill =(127, 127, 127))
        stnum = stnum + newim.size[1]

        
    im.save('{}/{}/{}/2.png'.format(poepildir, accountname, charname))


# In[25]:


#itemreqs('artctrl', 'smasham', '/home/pi/poe/')


# In[60]:


def creategif(accountname, charname):
    images = list()
    charpngs = os.listdir('{}/{}/{}'.format(poepildir, accountname, charname))
    for filen in charpngs:
        fils = '{}/{}/{}/{}'.format(poepildir, accountname, charname, filen)
        images.append(imageio.imread(fils))
    savegifd = '{}/{}-{}.gif'.format(poepildir, accountname, charname)
    imageio.mimsave(savegifd, images, fps=3)
    return('{}/static/{}-{}.gif'.format(ipaddr, accountname, charname))


# In[27]:


#creategif('artctrl', 'smasham', '/home/pi/poe/')


# In[28]:


#dacol.insert_one


# In[29]:


#colclass = poechar.c


# In[31]:


collectspec = db['poespecial']


# In[32]:


#collectspec.drop()


# In[33]:


#collectspec.update_many


# In[34]:


def specials():
    collectspec.drop()
    reqspeca = (requests.get('https://www.pathofexile.com/api/shop/microtransactions/specials').json()['entries'])
    #reqspeca
    for specitems in reqspeca:

        specitems.update({'localfile' : '{}{}{}'.format('/home/pi/poe/', specitems['microtransaction']['id'], '.png')})
    collectspec.insert_many(reqspeca)


# In[35]:


#specials()


# In[ ]:


#/home/pi/git/poepillow/poepillow/static


# In[38]:


import json


# In[48]:


def getcharpassive(accountname, charname, outdir):
    #specify poe account name, character, and local output directory a
    #saves the data to mongodb. 
    #getchar('artctrl', 'smasham', '/home/pi/poe/')
    charpass = requests.get('https://www.pathofexile.com/character-window/get-passive-skills?character={}&accountName={}'.format(charname, accountname)).json()
    return(charpass)
    #for charitems in charpass['items']:

    #    charitems.update({'localfile' : '{}{}{}'.format(outdir, charitems['id'], '.png')})
    #filter_dict = {'character.name' : charname}
    #if collection.count(filter_dict):
    #    collection.update_one(filter_dict, {"$set": charprof})
    #else:
    #    collection.insert_one(charprof)
    #    print("item is not existed")


# In[49]:


#charpassiv = getcharpassive('artctrl', 'swiftkik', '/home/pi/poe/')['hashes']


# In[50]:


import os


# In[52]:


def generatemicrogif():
    somesize = list()


    for coln in collectspec.find():
        response = requests.get(coln['imageUrl'], stream=True)
        with open('/home/pi/poe/{}.png'.format(coln['microtransaction']['id']), 'wb') as out_file:
            shutil.copyfileobj(response.raw, out_file)
            del response

        #somesize = list()
        newim = Image.open(coln['localfile'])
        somesize.append(newim.size[1])
    totsize = sum(somesize)
    #print(somesize)
    #print(totsize)

    im = Image.new(mode = "RGB", color='white', size = (500, totsize + 266) )
    d1 = ImageDraw.Draw(im)
    myFont = ImageFont.truetype(config['DEFAULT']['fontdir'], 14)
    titfont = ImageFont.truetype(config['DEFAULT']['fontdir'], 48)
        #charpic = Image.open(outdir + str(chard['character']['classId']) + '.png')
        #im.paste(charpic, (300, 10))
    d1.text((10, 10), 'daily specials'.upper(), font=titfont, fill =(127, 127, 127))
        #d1.text((10, 80), fulltitle.upper(), font=myFont, fill =(127, 127, 127))
    stnum = 266
    for coln in collectspec.find():
            #print(chari['name'])


            #print(chari['localfile'])
        newim = Image.open(coln['localfile'])
            #print(newim.size[1])
            #print(tots)
            #print (is_not_blank(chari['name']))
            #print(stnum)
        im.paste(newim, (10, stnum))
            #if is_not_blank(chari['name']) == 'False':
                #chari['name']:


        stnum = stnum + newim.size[1]


            #adgo.append(stnum)
            #totsize.append(im.size[1])
    im.save('{}{}.png'.format('/home/pi/poe/', 'special'))
    return('{}{}.png'.format(ipaddr, ':8888/', 'special'))


# In[53]:


#for spec in specials['entries']:
#    print(spec)
#    startime = arrow.get(spec['startAt'])
#    endtime = arrow.get(spec['endAt'])
#    timenow = arrow.now()
#    diff =  endtime - timenow
#    days = diff.days # Get Day 
#    hours,remainder = divmod(diff.seconds,3600) # Get Hour 
#    minutes,seconds = divmod(remainder,60) # Get Minute & Second 
#    print('Ends in: ')
#    print (days, " Days, ", hours, " Hours, ", minutes, " Minutes, ", seconds, " Second")
    #print(diff)
#    print('Sale Started: ')
#    print(startime.humanize())
#    print(endtime.humanize())
#    print('the sale price is: ' + str(spec['cost']))
#    print('the original price is: ' + str(spec['microtransaction']['cost']))
    #url = 'http://example.com/img.png'
#    nameitem = slugify(spec['microtransaction']['name'])
    #response = requests.get(spec['imageUrl'], stream=True)
    #print(spec['imageUrl'][-4:])
    #with open('/home/pi/poe/{}'.format(nameitem + spec['imageUrl'][-4:]), 'wb') as out_file:
    #    shutil.copyfileobj(response.raw, out_file)
    #del response


# In[54]:


def getpublicstash():
    return(requests.get('http://api.pathofexile.com/public-stash-tabs').json())


# In[57]:


headers = {
'User-Agent': 'My User Agent 1.0',
'From': 'youremail@domain.com'  # This is another valid field
}


# In[58]:


def getstashTab():
    headers = {
    'User-Agent': 'Mozilla/5.0 (X11; Linux armv7l) AppleWebKit/537.36 (KHTML, like Gecko) Raspbian Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36',
    'from': 'hammersmake@gmail.com'  # This is another valid field
    }

    response = requests.get('http://www.pathofexile.com/character-window/get-stash-items?accountName=artctrl&league=Ritual', headers=headers)
    return(response.json())

